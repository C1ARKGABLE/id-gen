package main

import (
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

var corpus = []rune("abcdefghijklmnpqrstuvwxyz123456789") // Alpha numeric with no O and 0
var corpusLen = len(corpus)
var logFlag *string

// RandomString ... This is to get the linter to shut up
func RandomString(n int) string {

	randString := make([]rune, n)
	for i := range randString {
		randString[i] = corpus[rand.Intn(corpusLen)]
	}
	return strings.ToUpper(string(randString))
}
func homePage(w http.ResponseWriter, r *http.Request) {

	var query = 5

	result := RandomString(query)

	fmt.Fprintf(w, result)

	// log.Println(result)

	log.Println(r.RemoteAddr, r.UserAgent(), result)
	// log.SetOutput(setLogFile(logFlag))
}

func handleRequests(portFlag int) {
	http.HandleFunc("/", homePage)

	fmt.Println("Using port: " + fmt.Sprint(portFlag))
	log.Fatal(http.ListenAndServe(":"+fmt.Sprint(portFlag), nil))
}

// func setLogFile(logFlag *string) *os.File {

// 	year, month, day := time.Now().Date()

// 	var splits = strings.Split(*logFlag, "/")

// 	splits[len(splits)-1] = fmt.Sprint(year, "-", int(month), "-", day, "_", splits[len(splits)-1])

// 	dateFile := strings.Join(splits, "/")

// 	f, err := os.OpenFile(dateFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
// 	if err != nil {
// 		log.Fatalf("Error opening file: %v", err)
// 	}
// 	defer f.Close()

// 	return f

// }
func main() {
	var portFlag = flag.Int("port", 4242, "Specify the port that the server listens on")

	rand.Seed(time.Now().UTC().UnixNano())

	logFlag = flag.String("log", "server.log", "Specify the log output file")

	flag.Parse()

	// log.SetOutput(setLogFile(logFlag))

	handleRequests(*portFlag)
}
